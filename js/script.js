$(document).ready(function(){
    $('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
    });
    $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
    });
	$('ul.tabs').delegate('li:not(.current)', 'click', function() {
		$(this).addClass('current').siblings().removeClass('current')
			   .parents('div.section').find('div.box').css({'opacity' : '0' , 'z-index' : '0'}).eq($(this).index()).css({'opacity' : '1' , 'z-index' : '1'});
	});
    $('.history_container ul.tabs').delegate('li:not(.current)', 'click', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
    });

    $('a.open_popup').click(function(e){
        $('body').addClass('noscroll');
        e.stopPropagation();
    });
    $('.popup').hide();
    $('.popup').find('.close').click(function(e){
        $('.popup').fadeOut(600);
        $('body').removeClass('noscroll');
        e.preventDefault();
    });
    $('.popup').click(function(){
         $(this).fadeOut(600);
         $('body').removeClass('noscroll');
    });
    $('.popup-content').click(function(e){
        e.stopPropagation();
    });
    $('a[href^="#go"], a[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
            var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
            if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
            }
            return false; // выключаем стандартное действие
        });
});

function PopUpShow(){
        $("#contact_popup").fadeIn(600);
    };
